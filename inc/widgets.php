<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gasthoeve_widgets_init() {
	register_sidebar( array(
        'name' => __( 'Contact', 'gasthoevebest' ),
        'id' => 'sidebar1',
        'description' => __( 'The contact data', 'gasthoevebest' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
        'after_widget' => '</div>',
	) );

	register_sidebar( array(
        'name' => __( 'Prijzen', 'gasthoevebest' ),
        'id' => 'sidebar2',
        'description' => __( 'The is for prijzen details', 'gasthoevebest' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
        'after_widget' => '</div>',
	) );
	
	register_sidebar( array(
        'name' => __( 'Likes On Op', 'gasthoeve' ),
        'id' => 'sidebar3',
        'description' => __( 'For Likes Ons Op Sociale Media', 'gasthoeve' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        'after_widget' => '</div>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gasthoeve' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gasthoeve' ),
		'before_widget' => '<section id="%1$s" class="widget card %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title card-header">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'gasthoeve_widgets_init' );
