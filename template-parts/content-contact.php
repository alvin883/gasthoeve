<?php
/**
 * Template part for displaying page `Contact`
 * 
 * Template Name: Contact Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gasthoeve
 */
get_header(); 


if(have_posts()) :
    while(have_posts()) : the_post(); ?>

	<div id="contact">
		<div id="content">
            <div class="section section-1" <?php 
                if( has_post_thumbnail() ){ 
                    echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '  \')"';
                }
            ?>>
                    <div class="container">
                        <h1 class="title">
                            <?php the_title(); ?>
                        </h1>
                        <h3 class="subtitle">
                            <?php the_field('subtitle_header'); ?>
                        </h3>
                    </div>
                </div>
			<div class="section section-2">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-8">
							<div id="the-content">
								<?php the_content(); ?>
                            </div>
                            <hr>
                            <?php if (get_field('title_maps') && get_field('embed_tag')) : ?>
                                <div class="maps">
                                    <h3 class="title">
                                        <?php the_field('title_maps'); ?>
                                    </h3>
                                    <div class="the-iframe">
                                        <?php the_field('embed_tag'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
						</div>
						<div class="col-12 col-lg-4">
                            <?php include get_stylesheet_directory() . '/template-parts/component-widget.php'; ?>
                        </div>
					</div>
				</div>
            </div>
		</div>
	</div>

<?php
	endwhile;
endif;
get_footer();