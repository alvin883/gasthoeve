
                            <div class="widget">
                                <div class="weather">
                                    <div class="loading">
                                        <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                            <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                                        </svg>
                                        <span>
                                            Loading Weather
                                        </span>
                                    </div>
                                    <div class="error">
                                        <i class="far fa-sad-tear icon"></i>
                                        <span>
                                            Sorry, theres a problem. Please refresh your browser ..
                                        </span>
                                    </div>
                                    <div class="top animate">
                                        <div>
                                            <div class="title">
                                                Elsendorp, Netherland
                                            </div>
                                            <div class="subtitle"></div>
                                            <div class="detail"></div>
                                        </div>
                                        <div>
                                            <span class="temperature"></span><sup>&#176;C</sup>
                                        </div>
                                    </div>
                                    <hr class="animate">
                                    <div class="bottom animate"></div>
                                </div>
                            </div>