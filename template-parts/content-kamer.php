<?php
/**
 * Template part for displaying page 'Reserveren'
 * 
 * Template Name: Single Kamer Page
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gasthoeve
 */

?>

<?php get_header();

if(have_posts()) :
    while(have_posts()) :the_post(); ?>

	<div id="overons">
		<div id="content">
            <div class="section section-1" <?php if( has_post_thumbnail() ){ 
                    echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                } ?>>
                <div class="container">
                    <h1 class="title">
                        <?php the_title(); ?>
                    </h1>
                </div>
            </div>
			<div class="section section-2">
				<div class="container">
					<div class="row">
                        <!-- `the_content` goes here -->
						<div class="col-12 col-lg-8">
                            <?php
                                    $lang = $_SERVER['REQUEST_URI']; 
                                    $whichLang = explode("=",$lang)[1];
                                ?>
                                <label class="selectLang">
                                    <i class="fas fa-globe icon"></i>
                                    <select>
                                        <option value="?lang=net" <?php if($whichLang != "ger" || $whichLang != "eng") echo "selected"; ?>>
                                            Netherlands
                                        </option>
                                        <option value="?lang=ger" <?php if($whichLang == "ger") echo "selected"; ?>>
                                            German
                                        </option>
                                        <option value="?lang=eng" <?php if($whichLang == "eng") echo "selected"; ?>>
                                            English
                                        </option>
                                    </select>
                                    <i class="fas fa-angle-down icon"></i>
                                </label>
							<div id="the-content">
                                <?php
                                    if($whichLang == "en") {
                                        the_field('content_en');
                                    } elseif ($whichLang== "ger") {
                                        the_field('content_ger');
                                    } else {
                                        the_field('content_net');
                                    }
                                ?>
							</div>
                            <hr>
                            <div class="other-room">
                                <div class="title">
                                    Other rooms you may like:
                                </div>
                                <div class="room-list row">
                                    <!-- Dynamic -->
                                    <?php 
                                    $currentpage = get_the_ID();
                                    $child_page = new WP_Query(array(
                                           'child_of' => $post->ID,
                                           'post_parent' => $post->post_parent,
                                           'post_type' => 'page',
                                           'sort_order' => 'desc',
                                           'post__not_in' => array(get_the_ID())
                                        ));  
                                        if ($child_page->have_posts()) :
                                            while ($child_page->have_posts()) : $child_page->the_post(); 
                                    ?>
                                                <div class="room-thumbnail col-12 col-lg-4 col-md-6">
                                                    <div class="item">
                                                        <div class="photo" <?php if( has_post_thumbnail() ){ 
                                                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"'; } ?>></div>
                                                        <div class="title">
                                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile;
                                        endif; ?>
                                    <!-- END Dynamic -->  
                                </div>
                            </div>
                        </div>
                        <!-- END of `the_content` -->

						<div class="col-12 col-lg-4">
                            <?php include get_stylesheet_directory() . '/template-parts/component-widget.php'; ?>
                        </div>
					</div>
				</div>
            </div>
		</div>
	</div>

    <?php endwhile; ?>
<?php endif; ?>

<?php   
get_footer(); ?>