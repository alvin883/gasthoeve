<?php
/**
 * Template part for displaying page `Impressie`
 * 
 * Template Name: Impression Page
 * 
 */
get_header();


if(have_posts()) :
    while(have_posts()) : the_post(); ?>

	<div id="contact" class="impressie">
		<div id="content">
            <div class="section section-1">
                    <div class="container">
                        <h1 class="title">
                            <?php the_title(); ?>
                        </h1>
                        <h3 class="subtitle">
                            <?php the_field('subtitle_header'); ?>
                        </h3>
                    </div>
                </div>
			<div class="section section-2">
				<div id="the-content">
					<?php the_content(); ?>
				</div>
            </div>
		</div>
	</div>

<?php
	endwhile;
endif;
get_footer();