<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gasthoeve
 */

?>
<?php include get_stylesheet_directory() .'/includes/privacy.php'; ?>
	<footer id="footer">
		<div class="footer footer-1">
			<div class="container">
				<div class="row">
					<div class="column col-12 col-sm-6 col-lg-3">
						<div class="title">
							Contact Gegevens
						</div>
						<div class="content">
							<table>
								<tr>
									<td>
										<i class="fas fa-map-marker-alt"></i>
									</td>
									<td>
										<?php the_field('address', 'option'); ?>
									</td>
								</tr>
								<tr>
									<td>
										<i class="fas fa-phone"></i>
									</td>
									<td>
										<?php the_field('phone', 'option'); ?>
										<br/>
										<?php the_field('email', 'option'); ?>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<?php if(get_field('title_prijzen', 'option') && get_field('content_prijzen', 'option')) : ?>
						<div class="column col-12 col-sm-6 col-lg-3">
							<div class="title">
								<?php the_field('title_prijzen', 'option'); ?>
							</div>
							<div class="content">
								<?php the_field('content_prijzen', 'option'); ?>
							</div>
							<a href="<?php get_field('button_rooms','option'); ?>" class="btn">Kamers</a>
						</div>
					<?php endif; ?>

					<div class="column col-12 col-lg-6">
						<div class="title">
							Volg ons op facebook
						</div>
						<div class="content biggest">
							<div>
								<?php echo get_field('facebook_iframe', 'option'); ?> 
							</div>
							<div>
								<?php if(have_rows('picture_repeater', 'option')) :
									while(have_rows('picture_repeater', 'option')) : the_row(); 
										$image = get_sub_field('picture', 'option'); ?>
											<a target="_blank" href="<?php echo get_sub_field('link_refers_to','option'); ?>" class="neighborhood"
												style="background-image: url('<?php echo $image['url']; ?>');">
											</a>
									<?php endwhile;
								endif; ?>
							</div>
						</div><!--.Content-biggest -->
						<a class="btn js_popup-activator">Goed om te weten</a>
					</div><!--.column col-12 col-lg-6-->
				</div>
				<a href="#privacy" class="js_popup-activator2">Privacy Statement</a>
			</div>
		</div>

		<div class="footer footer-4">
			<div class="container">
				<div class="left">
					<div class="small">
						<?php if ( get_field ('instagram', 'option') ): ?>
							<a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fab fa-instagram"></i></a>
						<?php endif; ?>
						<?php if ( get_field ('twitter', 'option') ): ?>
							<a href="<?php the_field('twitter', 'option'); ?>" target="_blank"><i class="fab fa-twitter"></i></a>
						<?php endif; ?>
						<?php if ( get_field ('linked_in', 'option') ): ?>
							<a href="<?php the_field('linked_in', 'option'); ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
						<?php endif; ?>
						<?php if ( get_field ('facebook', 'option') ): ?>
							<a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fab fa-facebook"></i></a>
						<?php endif; ?>
					</div>
					<i class="far fa-copyright"></i>
					<b>
						<?php echo date("Y"); ?>
						<a href="<?php echo site_url(); ?>">Gasthoevededompt</a>
					</b>
					- Designed by <b>Wappstars B.V.</b>
				</div>
				<div class="right">
					<?php if ( get_field ('instagram', 'option') ): ?>
						<a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fab fa-instagram"></i></a>
					<?php endif; ?>
					<?php if ( get_field ('twitter', 'option') ): ?>
						<a href="<?php the_field('twitter', 'option'); ?>" target="_blank"><i class="fab fa-twitter"></i></a>
					<?php endif; ?>
					<?php if ( get_field ('linked_in', 'option') ): ?>
						<a href="<?php the_field('linked_in', 'option'); ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
					<?php endif; ?>
					<?php if ( get_field ('facebook', 'option') ): ?>
						<a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fab fa-facebook"></i></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</footer>
	<?php include get_stylesheet_directory() . '/includes/neighborhood.php'; ?>
</body>
</html>