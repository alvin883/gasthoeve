jQuery(document).ready(function($){
    
    // Navbar, Toggle small navigation when user scroll
    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        if(scroll > 100){
            $("#navbar .container.bottom").addClass("small");
        }else{
            $("#navbar .container.bottom").removeClass("small");
        }
    });

    $( ".selectLang select" ).change(function() {
        var url = (document.URL).split("?")[0] + $(this).val();
        location.href = url;
    });

    // Guestbook Pop Up
    $(".js_popup-activator").click(function(){
        $("#neighborhood").addClass("show");
    });
    $(".js_popup-deactivator").click(function(){
        $("#neighborhood").removeClass("show");
    });

    //Privacy Statement
    $(".js_popup-activator2").click(function(){
        $("#privacy").addClass("show");
    });

    $(".js_popup-deactivator2").click(function(){
        $("#privacy").removeClass("show");
    });

    // Widget
    var weatherSelector = $(".weather");
    if(weatherSelector.length){
        $.get("https://data.buienradar.nl/2.0/feed/json", function(data){
            var uden = data.actual.stationmeasurements.find(function(val){
                return val.regio.toLowerCase() == "uden";
            });
            var date = new Date(uden.timestamp),
                monthName = [
                    "januari","februari","maart","april","mei","juni",
                    "juli","augustus","september","oktober","november","december"
                ],
                usedData = {
                    date: date.getDate() +" "+ monthName[date.getMonth()] +" "+date.getFullYear(),
                    detail: uden.weatherdescription,
                    temp: uden.temperature,
                    icon: uden.iconurl,
                    nextDay: [{
                            name: "Morgen",
                            icon: data.forecast.fivedayforecast[0].iconurl
                        },
                        {
                            name: new Date(data.forecast.fivedayforecast[1].day).getDate()+" "+
                                    monthName[new Date(data.forecast.fivedayforecast[1].day).getMonth()].slice(0,3),
                            icon: data.forecast.fivedayforecast[1].iconurl
                        },
                        {
                            name: new Date(data.forecast.fivedayforecast[2].day).getDate()+" "+
                                    monthName[new Date(data.forecast.fivedayforecast[2].day).getMonth()].slice(0,3),
                            icon: data.forecast.fivedayforecast[2].iconurl
                        }]
            };

            // Render the data
            $(".weather .top div .subtitle").append(usedData.date);
            $(".weather .top div .detail").prepend('<img src="'+ usedData.icon +'" alt="" class="icon"></img><span class="detail-text">' + usedData.detail + '</span>');
            $(".weather .top div .temperature").append(usedData.temp);
            for( var item in usedData.nextDay){
                $(".weather .bottom").append('<div>'+
                        '<div class="date">'+ usedData.nextDay[item].name +'</div>'+
                        '<img src="'+ usedData.nextDay[item].icon +'" alt="" class="icon">'+
                    '</div>'
                );
            }
            $(".weather").addClass("loaded");
        }).fail(function(){
            $(".weather").addClass("error");
        });
    }

    /** Swipebox */
    $( '.gallery-native,.wp-block-gallery' ).each(function(i){
        var newClass= 'gallery_'+ i;
        $(this).addClass(newClass);
        $('.' + newClass + ' a').swipebox();
    });
});