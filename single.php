<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gasthoeve
 */

get_header();

if(have_posts()) :
    while(have_posts()) : the_post(); ?>

<div id="overons">
    <div id="content">
        <div class="section section-1" <?php 
            if( has_post_thumbnail() ){ 
                echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
            } 
        ?>>
        <div class="container">
            <h1 class="title">
                <?php the_title(); ?>
            </h1>
            <h5 class="subtitle">
				<?php the_time('F j, Y'); ?>
            </h5>
        </div>
    </div>

        <div class="section section-2">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div id="the-content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <?php include get_stylesheet_directory() . '/template-parts/component-widget.php'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <?php endwhile;
else:
    echo "Sorry, no post were found";
endif; ?>
<?php 
get_footer();