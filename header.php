<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Korenbest
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<nav class="navbar navbar-expand-xl fixed-top" id="navbar">
		<div class="top">
			<div class="container">
				<div class="left">
					<i class="fas fa-bed"></i>
					<span class="hide">Groet goep ? </span><a href="<?php the_field('refer_header', 'option'); ?>"><?php the_field('slug_title', 'option'); ?></a>
				</div>
				<div class="right">
					<a href="mailto:<?php the_field('email', 'option'); ?>">
						<i class="fas fa-envelope"></i><div class="hide"><?php the_field('email', 'option'); ?></div>
					</a>
					<a href="tel:<?php the_field('phone', 'option'); ?>">
						<span class="rotate"><i class="fas fa-phone"></i></span><div class="hide"><?php the_field('phone', 'option'); ?></div>
					</a>
				</div>
			</div>
		</div>

		<div class="container bottom">
			<a href="<?php echo site_url(); ?>" class="a-logo"> 
				<?php $image = get_field('logo', 'option'); ?>
				<img class="custom-logo" src="<?php echo $image['url']; ?>">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fas fa-bars"></i>
			</button>
			
			<div class="right-side navbar-collapse collapse" id="navbarContent">
				<?php
					$args = array(
						'theme_location' => 'primary',
						'depth'      => 2,
						'container'  => false,
						'menu_class'     => 'navbar-nav',
						'walker'     => new Bootstrap_Walker_Nav_Menu()
						);
					if (has_nav_menu('primary')) {
						wp_nav_menu($args);
					}
				?>
			</div>
		</div>
	</nav>

	