<div class="section section-2">
    <?php if(get_field('title_about')) : ?>
        <div class="container">
            <div class="data">
                <div class="title">
                    <?php the_field('title_about'); ?> 
                </div>
                <div class="content">
                    <?php the_field('about'); ?>
                </div>
                <a href="<?php echo esc_url(get_field('referring_about')); ?>" class="btn">
                    Less meer
                </a>
            </div>
            <?php $image = get_field('image_about'); ?>
            <div class="photo" style="background-image: url('<?php echo $image['url']; ?>')"></div>
        </div><!--.container -->
    <?php endif; ?>
</div>

