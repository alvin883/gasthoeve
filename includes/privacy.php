<div id="privacy">
    <div class="paper">
        <div class="header"> 
            <h4 class="title">
                Privacy Statement
            </h4>
            <button class="close-button js_popup-deactivator2">
                <i class="far fa-times-circle"></i>
            </button>
        </div>
        <div class="body">
            <div class="content" id="the-content">
                <?php the_field('privacy', 'option'); ?>
            </div>
        </div>
    </div>
</div>