

	<div id="neighborhood">
		<div class="paper">
			<div class="header">
				<h4 class="title">
					Neighborhood
				</h4>
				<button class="close-button js_popup-deactivator">
					<i class="far fa-times-circle"></i>
				</button>
			</div>
			<div class="body">
				<div class="content" id="the-content">
					<?php the_field('neighborhood', 'option'); ?>
				</div>
			</div>
		</div>
	</div>