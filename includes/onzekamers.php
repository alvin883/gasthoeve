<div class="section section-4">
    <div class="container">
        <!--To Display image in columns in the homepage-->
        <?php if(get_field('title_kamers')) : ?>
            <div class="title">
                <?php the_field('title_kamers'); ?> 
            </div>
        <?php endif; ?>

        <div class="content">
            <?php if(have_rows('room_photos')) : 
                $i = 0;
                $close = count(get_field('room_photos')) % 3;
                $sum_row = $close ? (int) (count(get_field('room_photos'))/3) + 1 : count(get_field('room_photos')) /3;
                $i2 =0;
                while(have_rows('room_photos')) : the_row(); 
                    $i++;
                    if($i == 1 ): 
                        $i2++; ?>
                        <div class="row" <?php if ($i2 == $sum_row && $close): ?> style="justify-content: space-around;" <?php endif; ?>>
                    <?php endif; ?>
                        <div class="column col-12 col-lg-3">
                            <div class="item">
                                <?php $image = get_sub_field('photo'); ?>

                                <div class="photo"
                                    style="background-image: url('<?php echo $image['url']; ?>');"></div>
                                <div class="title">
                                    <a href="<?php the_sub_field('referring_photos'); ?>"><?php echo get_sub_field('rooms_title'); ?></a>
                                </div>

                            </div>
                        </div>

                    <?php if ($i == 3) : $i=0; ?>
                        </div><!--row-->
                    <?php endif;?>
                    
                <?php endwhile; 
                    if ($close>0) :?>
                        </div><!--row-->
                    <?php endif;

            endif; ?>
            
        </div><!--content-->
    </div><!--container-->
</div>
