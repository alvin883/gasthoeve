<?php
/**
 * @package Korenbest
 * @subpackage theme name here
 * Template Name: Homepage	
 */
?>
<?php get_header(); ?>
	<div id="homepage">
		<div id="content">

			<?php if(get_field('the_bussiness_name') && get_field('the_services') && get_field('image_brand')) : 
				$image = get_field('image_brand');  ?>
				<div class="section section-1" style="background-image: url('<?php echo $image['url']; ?>')">
					<div class="container">
						<h1 class="title">
							<?php the_field('the_bussiness_name'); ?>
						</h1>
						<h3 class="subtitle">
							<?php the_field('the_services'); ?>
						</h3>
						<a href="<?php the_field('link_refers_to'); ?>" class="btn">
							Boek nu
						</a>
					</div>
				</div>
			<?php endif; ?>

			<?php include get_stylesheet_directory() . '/includes/about.php'; ?>

			<?php if(get_field('image_promotion') && get_field('promotion_word')) : ?>
				<?php $image = get_field('image_promotion'); ?>
				<div class="section section-3" style="background-image: url('<?php echo $image['url']; ?>')">
					<div class="container">
						<?php the_field('promotion_word'); ?>
					</div>
				</div>
			<?php endif; ?>

			<?php include get_stylesheet_directory() . '/includes/onzekamers.php'; ?>
		</div>
	</div>

<?php 
get_footer();
